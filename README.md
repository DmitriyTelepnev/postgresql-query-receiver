# Postgres query receiver

## Ограничения
* Является реализацией логической репликации, не реплицирует структуру таблиц, индексы и все, что относится к DDL
* Не поддерживает первичные и внешние ключи, потому что перекачиваем данные пачками, на уровне приложения должны обращаться к составному первичному ключу(uid - node_name)
* Не поддерживает update, delete, truncate

## Настройка

```yaml
nodeName: "node-1"
downloadPeriod: "10s"
batchSize: 10000
destDirPath: "/tmp/data"

pgconn:
  host: "localhost"
  port: 5432
  dbname: "postgres"
  username: "postgres"
  password: "changeme"

log:
  level: info
```
**nodeName** - имя ноды, данные которой тянем

**downloadPeriod** - интервал, через который запускается скачивание данных

**batchSize** - размер пачки данных, которая перекачивается для каждой таблицы единовременно

**destDirPath** - путь, по которому будут лежать архивы данных

Секция **pgconn** отвечает за подключение к PostgreSQL

## Запуск

