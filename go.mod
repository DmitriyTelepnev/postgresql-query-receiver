module bitbucket.org/DmitriyTelepnev/postgresql-query-receiver

go 1.12

require (
	github.com/lib/pq v1.3.0
	github.com/pkg/errors v0.8.0
	github.com/spf13/viper v1.6.3
)
