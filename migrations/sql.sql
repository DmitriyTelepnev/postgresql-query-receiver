
CREATE OR REPLACE FUNCTION setNodeName() RETURNS trigger AS $$
BEGIN
  IF COALESCE(NEW.node_name, '') IS NOT DISTINCT FROM '' THEN
    NEW.node_name = 'node-1';
  END IF;

  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER setNodeNameTrg_test BEFORE INSERT ON test
  FOR EACH ROW EXECUTE PROCEDURE setNodeName();

ALTER TABLE <table> ADD COLUMN node_name VARCHAR(30);
CREATE INDEX ON <table> (node_name, id desc);


