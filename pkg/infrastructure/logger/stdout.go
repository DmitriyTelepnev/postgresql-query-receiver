package logger

import (
	"fmt"
	"os"
	"strings"
	"time"
)

const (
	ERROR = iota
	WARN
	INFO
	DEBUG
)

type LogLevel int

func NewLogLevel(level string) LogLevel {
	var l LogLevel
	switch strings.ToLower(level) {
	case "error":
		l = ERROR
		break
	case "warn":
		l = WARN
		break
	case "info":
		l = INFO
		break
	case "debug":
		l = DEBUG
		break
	}

	return l
}

type StdoutLogger struct {
	level LogLevel
}

func NewStdout(level string) *StdoutLogger {
	return &StdoutLogger{
		level: NewLogLevel(level),
	}
}

func (l *StdoutLogger) Info(msg string) {
	if l.level >= INFO {
		fmt.Println(fmt.Sprintf("%s [%s] %s", time.Now().Format(time.RFC3339Nano), "info", msg))
	}
}
func (l *StdoutLogger) Infof(pattern string, params ...interface{}) {
	if l.level >= INFO {
		fmt.Println(fmt.Sprintf("%s [%s] %s", time.Now().Format(time.RFC3339Nano), "info", fmt.Sprintf(pattern, params...)))
	}
}
func (l *StdoutLogger) Warn(msg string) {
	if l.level >= WARN {
		fmt.Println(fmt.Sprintf("%s [%s] %s", time.Now().Format(time.RFC3339Nano), "warn", msg))
	}
}
func (l *StdoutLogger) Warnf(pattern string, params ...interface{}) {
	if l.level >= WARN {
		fmt.Println(fmt.Sprintf("%s [%s] %s", time.Now().Format(time.RFC3339Nano), "warn", fmt.Sprintf(pattern, params...)))
	}
}
func (l *StdoutLogger) Debug(msg string) {
	if l.level >= DEBUG {
		fmt.Println(fmt.Sprintf("%s [%s] %s", time.Now().Format(time.RFC3339Nano), "debug", msg))
	}
}
func (l *StdoutLogger) Debugf(pattern string, params ...interface{}) {
	if l.level >= DEBUG {
		fmt.Println(fmt.Sprintf("%s [%s] %s", time.Now().Format(time.RFC3339Nano), "debug", fmt.Sprintf(pattern, params...)))
	}
}
func (l *StdoutLogger) Error(msg string) error {
	if l.level >= ERROR {
		err := fmt.Errorf("%s", msg)
		fmt.Println(fmt.Sprintf("%s [%s] %s", time.Now().Format(time.RFC3339Nano), "error", msg))
		return err
	}
	return nil
}
func (l *StdoutLogger) Errorf(pattern string, params ...interface{}) error {
	if l.level >= ERROR {
		err := fmt.Errorf("%s", fmt.Sprintf(pattern, params...))
		fmt.Println(fmt.Sprintf("%s [%s] %s", time.Now().Format(time.RFC3339Nano), "error", fmt.Sprintf(pattern, params...)))
		return err
	}
	return nil
}
func (l *StdoutLogger) Fatal(msg string) {
	fmt.Println(fmt.Sprintf("%s [%s] %s", time.Now().Format(time.RFC3339Nano), "fatal", msg))
	os.Exit(1)
}
func (l *StdoutLogger) Fatalf(pattern string, params ...interface{}) {
	fmt.Println(fmt.Sprintf("%s [%s] %s", time.Now().Format(time.RFC3339Nano), "fatal", fmt.Sprintf(pattern, params...)))
	os.Exit(1)
}
