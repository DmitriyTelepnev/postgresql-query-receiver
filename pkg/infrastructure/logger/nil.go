package logger

type NilLogger struct{}

func (l *NilLogger) Info(msg string)                                    {}
func (l *NilLogger) Infof(pattern string, params ...interface{})        {}
func (l *NilLogger) Warn(msg string)                                    {}
func (l *NilLogger) Warnf(pattern string, params ...interface{})        {}
func (l *NilLogger) Debug(msg string)                                   {}
func (l *NilLogger) Debugf(pattern string, params ...interface{})       {}
func (l *NilLogger) Error(msg string) error                             { return nil }
func (l *NilLogger) Errorf(pattern string, params ...interface{}) error { return nil }
func (l *NilLogger) Fatal(msg string)                                   {}
func (l *NilLogger) Fatalf(pattern string, params ...interface{})       {}
