FROM scratch

WORKDIR /usr/src/app
COPY ./bin/app .
COPY ./config.yaml .

ENTRYPOINT ["./app"]