package datasource

import (
	"database/sql"
	"fmt"

	"bitbucket.org/DmitriyTelepnev/postgresql-query-receiver/internal/infrastructure/config"
	_ "github.com/lib/pq"
	"github.com/pkg/errors"
)

func NewPostgresConn(cfg config.PgConnection) (*sql.DB, error) {
	dsn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", cfg.Host, cfg.Port, cfg.Username, cfg.Password, cfg.Dbname)
	conn, err := sql.Open("postgres", dsn)
	if err != nil {
		return nil, errors.Wrapf(err, "Unable to open connection to postgres. DSN: %s", dsn)
	}
	err = conn.Ping()
	if err != nil {
		return nil, errors.Wrapf(err, "Unable to ping connection to postgres. DSN: %s", dsn)
	}
	conn.SetMaxOpenConns(10)

	return conn, nil
}
