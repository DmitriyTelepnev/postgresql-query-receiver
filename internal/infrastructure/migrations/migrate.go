package migrations

import (
	"database/sql"
	"fmt"

	"bitbucket.org/DmitriyTelepnev/postgresql-query-receiver/pkg/infrastructure/logger"
)

const createTableOffsets = "CREATE TABLE IF NOT EXISTS _table_offsets (_tablename VARCHAR(70), _offset BIGINT);"
const createTableOffsetsIndex = "CREATE INDEX IF NOT EXISTS _table_offsets_idx ON _table_offsets (_tablename, _offset);"
const createSetNodeNameTrigger = `
CREATE OR REPLACE FUNCTION setNodeName() RETURNS trigger AS $$
	BEGIN
	IF COALESCE(NEW.node_name, '') IS NOT DISTINCT FROM '' THEN
		NEW.node_name = '%s';
	END IF;

	RETURN NEW;
	END;
$$ LANGUAGE plpgsql;
`

const getAllPublicTables = "SELECT tablename FROM pg_tables WHERE schemaname = 'public' and tablename not like '\\_%';"
const addBigserialId = "ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS _id bigserial NOT NULL;"
const addNodeName = "ALTER TABLE \"%s\" ADD COLUMN IF NOT EXISTS node_name VARCHAR(50);"
const addIndexOnIdAndNodeName = "CREATE INDEX IF NOT EXISTS %s_system_id_node_name ON \"%s\" (_id, node_name);"
const dropTriggers = "DROP TRIGGER IF EXISTS %s_set_node_name on \"%s\";"
const addTriggerOnNodeName = `
	CREATE TRIGGER %s_set_node_name BEFORE INSERT ON %s
	FOR EACH ROW EXECUTE PROCEDURE setNodeName();
`

const getTableIdColumn = "SELECT true FROM information_schema.columns WHERE table_name = $1 AND column_name = '_id';"

func MustUp(pgConn *sql.DB, nodeName string, logger logger.Logger) {
	logger.Debug("try to create _table_offsets")
	_, err := pgConn.Exec(createTableOffsets)
	panicOnErr(err)

	logger.Debug("try to create _table_offsets indexes")
	_, err = pgConn.Exec(createTableOffsetsIndex)
	panicOnErr(err)

	logger.Debug("try to create set_node_name trigger procedure")
	_, err = pgConn.Exec(fmt.Sprintf(createSetNodeNameTrigger, nodeName))
	panicOnErr(err)

	tables, err := pgConn.Query(getAllPublicTables)
	panicOnErr(err)
	for tables.Next() {
		var tableName string
		err = tables.Scan(&tableName)
		panicOnErr(err)

		if !tableIdExists(pgConn, tableName) {
			logger.Debugf("try to create _id for %s", tableName)
			_, err = pgConn.Exec(fmt.Sprintf(addBigserialId, tableName))
			panicOnErr(err)

			logger.Debugf("try to create node_name for %s", tableName)
			_, err = pgConn.Exec(fmt.Sprintf(addNodeName, tableName))
			panicOnErr(err)

			logger.Debugf("try to create _id/node-name index for %s", tableName)
			_, err = pgConn.Exec(fmt.Sprintf(addIndexOnIdAndNodeName, tableName, tableName))
			panicOnErr(err)

			logger.Debugf("try to recreate set_node_name trigger for %s", tableName)
			_, err = pgConn.Exec(fmt.Sprintf(dropTriggers, tableName, tableName))
			panicOnErr(err)

			_, err = pgConn.Exec(fmt.Sprintf(addTriggerOnNodeName, tableName, tableName))
			panicOnErr(err)
		} else {
			logger.Debugf("_id exists for %s", tableName)
		}
	}
}

func tableIdExists(pgConn *sql.DB, tableName string) bool {
	res := sql.NullBool{Bool: false}
	r := pgConn.QueryRow(getTableIdColumn, tableName)
	err := r.Scan(&res)
	if err != nil && err != sql.ErrNoRows {
		panicOnErr(err)
	}

	return res.Bool
}

func panicOnErr(err error) {
	if err != nil {
		panic(err)
	}
}
