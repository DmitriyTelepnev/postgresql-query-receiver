package service

import (
	"archive/tar"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"time"

	"bitbucket.org/DmitriyTelepnev/postgresql-query-receiver/internal/domain/entity"
)

type FileWriter struct {
	nodeName string
	fileName string
	path     string
}

func NewFileWriter(nodeName string) *FileWriter {
	return &FileWriter{nodeName: nodeName}
}

func (w *FileWriter) SetDestDataPath(path string) error {
	dir, err := os.Stat(path)
	if err != nil && dir == nil {
		err = os.Mkdir(path, 0777)
		if err != nil {
			return err
		}
	}
	if err != nil {
		return err
	}
	if !dir.IsDir() {
		return fmt.Errorf("%s must be dir", path)
	}

	w.path = path
	return nil
}

func (w *FileWriter) CreateCurrentDataDir() (string, error) {
	currentDataDirPath := fmt.Sprintf("%s/%d_%s", w.path, time.Now().Unix(), w.nodeName)
	dir, err := os.Stat(currentDataDirPath)
	if err != nil && dir == nil {
		err = os.Mkdir(currentDataDirPath, 0777)
		if err != nil {
			return "", err
		}
		return currentDataDirPath, nil
	}
	if err != nil {
		return "", err
	}
	if !dir.IsDir() {
		return "", fmt.Errorf("%s must be dir", currentDataDirPath)
	}
	return currentDataDirPath, nil
}

func (w *FileWriter) WriteTable(dataDir string, table *entity.Table) error {
	f, err := os.Create(fmt.Sprintf("%s/%s", dataDir, table.Name))
	if err != nil {
		return err
	}
	_, err = fmt.Fprintln(f, string(table.Data))
	err = f.Close()
	if err != nil {
		return err
	}
	return nil
}

func (w *FileWriter) Tar(dataDir string) error {
	_, err := os.Stat(dataDir)
	defer os.RemoveAll(dataDir)

	if err != nil {
		return fmt.Errorf("Unable to tar files - %v", err.Error())
	}

	tarfile, err := os.Create(fmt.Sprintf("%s.tar", dataDir))
	if err != nil {
		return err
	}
	defer tarfile.Close()

	files, err := ioutil.ReadDir(dataDir)
	if err != nil {
		return err
	}

	if len(files) == 0 {
		return nil
	}

	var fileWriter io.WriteCloser = tarfile

	tarfileWriter := tar.NewWriter(fileWriter)
	defer tarfileWriter.Close()

	for _, fileInfo := range files {
		if fileInfo.IsDir() {
			continue
		}

		path := dataDir + string(filepath.Separator) + fileInfo.Name()
		file, err := os.Open(path)
		if err != nil {
			return fmt.Errorf("cant open file %s for tar", path)
		}
		defer file.Close()

		header := new(tar.Header)
		header.Name = fileInfo.Name()
		header.Size = fileInfo.Size()
		header.Mode = int64(fileInfo.Mode())
		header.ModTime = fileInfo.ModTime()

		err = tarfileWriter.WriteHeader(header)
		if err != nil {
			return err
		}

		_, err = io.Copy(tarfileWriter, file)
		if err != nil {
			return err
		}
	}

	return nil
}
