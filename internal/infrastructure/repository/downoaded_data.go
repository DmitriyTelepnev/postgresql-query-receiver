package repository

import (
	"database/sql"
	"fmt"

	"bitbucket.org/DmitriyTelepnev/postgresql-query-receiver/pkg/infrastructure/logger"
)

type DownloadedData struct {
	conn   *sql.DB
	logger logger.Logger
}

func NewDownloadedData(conn *sql.DB, logger logger.Logger) *DownloadedData {
	return &DownloadedData{conn, logger}
}

const getLatestDownloadedIdByTable = "SELECT MAX(_offset) _offset FROM _table_offsets WHERE _tablename = $1"

func (r *DownloadedData) GetLatestDownloadedIdByTable(tableName string) (int64, error) {
	offset := sql.NullInt64{Int64: 0}
	row := r.conn.QueryRow(getLatestDownloadedIdByTable, tableName)
	scanErr := row.Scan(&offset)

	return offset.Int64, scanErr
}

const setLatestDownloadedIdByTable = "INSERT INTO _table_offsets (_tablename, _offset) VALUES ($1, $2)"

func (r *DownloadedData) SetLatestDownloadedIdByTable(tableName string, offset int64) error {
	res, err := r.conn.Exec(setLatestDownloadedIdByTable, tableName, offset)
	if res != nil {
		affectedRows, _ := res.RowsAffected()
		if affectedRows == 0 {
			return fmt.Errorf("offset %d not setted for %s", offset, tableName)
		}
	}
	return err
}
