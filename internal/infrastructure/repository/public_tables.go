package repository

import (
	"database/sql"

	"bitbucket.org/DmitriyTelepnev/postgresql-query-receiver/internal/domain/entity"
	"bitbucket.org/DmitriyTelepnev/postgresql-query-receiver/pkg/infrastructure/logger"
)

type PublicTables struct {
	conn   *sql.DB
	logger logger.Logger
}

func NewPublicTables(conn *sql.DB, logger logger.Logger) *PublicTables {
	return &PublicTables{conn, logger}
}

const getAllPublicTables = "SELECT tablename FROM pg_tables WHERE schemaname = 'public' and tablename not like '\\_%';"

func (r *PublicTables) GetAll() ([]*entity.Table, error) {
	rows, queryErr := r.conn.Query(getAllPublicTables)
	if queryErr != nil {
		return nil, queryErr
	}

	var scanErr error
	tables := []*entity.Table{}
	for rows.Next() {
		table := &entity.Table{}
		scanErr = rows.Scan(&table.Name)
		if scanErr != nil {
			return nil, scanErr
		}
		tables = append(tables, table)
	}
	return tables, nil
}
