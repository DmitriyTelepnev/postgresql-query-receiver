package repository

import (
	"database/sql"
	"fmt"

	"bitbucket.org/DmitriyTelepnev/postgresql-query-receiver/internal/domain/entity"
	"bitbucket.org/DmitriyTelepnev/postgresql-query-receiver/pkg/infrastructure/logger"
)

type TableRows struct {
	conn     *sql.DB
	nodeName string
	logger   logger.Logger
}

func NewTableRows(conn *sql.DB, nodeName string, logger logger.Logger) *TableRows {
	return &TableRows{conn, nodeName, logger}
}

const getWithLimitOffset = "SELECT max(_id) as latestOffset, json_agg(_t) as data FROM (SELECT * FROM \"%s\" WHERE _id > $1 and (node_name IN ($2, '') or node_name IS NULL) ORDER BY _id LIMIT $3) _t;"

func (r *TableRows) GetWithLimitOffset(table *entity.Table, offset int64, limit int64) error {
	latestOffset := sql.NullInt64{Int64: 0}
	data := sql.NullString{String: ""}

	row := r.conn.QueryRow(
		fmt.Sprintf(getWithLimitOffset, table.Name),
		offset,
		r.nodeName,
		limit,
	)

	err := row.Scan(&latestOffset, &data)
	if err != nil {
		return err
	}

	if data.String == "" {
		return nil
	}

	table.LatestOffset = latestOffset.Int64
	table.Data = data.String

	return nil
}
