package app

import (
	"os"
	"sync"
	"sync/atomic"
	"time"

	"bitbucket.org/DmitriyTelepnev/postgresql-query-receiver/internal/domain/entity"
	"bitbucket.org/DmitriyTelepnev/postgresql-query-receiver/internal/domain/repository"
	"bitbucket.org/DmitriyTelepnev/postgresql-query-receiver/internal/domain/service"
	"bitbucket.org/DmitriyTelepnev/postgresql-query-receiver/pkg/infrastructure/logger"
)

type receiver struct {
	downloadPeriod     time.Duration
	batchSize          int64
	publicTablesRepo   repository.PublicTables
	downloadedDataRepo repository.DownloadedData
	tableRowsRepo      repository.TableRows
	fileWriter         service.FileWriter
	logger             logger.Logger
	isStopped          chan bool
}

func NewReceiver(
	downloadPeriod time.Duration,
	batchSize int64,
	publicTablesRepo repository.PublicTables,
	downloadedDataRepo repository.DownloadedData,
	tableRowsRepo repository.TableRows,
	fileWriter service.FileWriter,
	logger logger.Logger,
) *receiver {
	return &receiver{
		downloadPeriod:     downloadPeriod,
		batchSize:          batchSize,
		publicTablesRepo:   publicTablesRepo,
		downloadedDataRepo: downloadedDataRepo,
		tableRowsRepo:      tableRowsRepo,
		fileWriter:         fileWriter,
		logger:             logger,
		isStopped:          make(chan bool),
	}
}

func (s *receiver) Run() error {
	ticker := time.NewTicker(s.downloadPeriod)
	for {
		select {
		case <-s.isStopped:
			ticker.Stop()
			return nil
		case <-ticker.C:
			err := s.doWork()
			if err != nil {
				return err
			}
		default:
			break
		}
	}
}

func (s *receiver) doWork() error {
	tables, getTablesErr := s.publicTablesRepo.GetAll()
	if getTablesErr != nil {
		return getTablesErr
	}

	dataDir, err := s.fileWriter.CreateCurrentDataDir()
	if err != nil {
		return err
	}
	defer os.Remove(dataDir)

	downloadTablesWg := sync.WaitGroup{}
	var isNeedTar atomic.Value
	isNeedTar.Store(false)
	for _, table := range tables {
		downloadTablesWg.Add(1)
		go func(table *entity.Table, dataDir string) {
			s.downloadData(table)
			s.logger.Debugf("%s downloading, current offset %d", table.Name, table.LatestOffset)
			if table.Data != "" {
				s.logger.Debugf("try write \"%s\" data into path %s", table.Name, dataDir)
				err := s.fileWriter.WriteTable(dataDir, table)
				if err != nil {
					s.logger.Errorf("%v", err)
				}
				s.setOffset(table)
				isNeedTar.Store(true)
			}
			downloadTablesWg.Done()
		}(table, dataDir)
	}

	downloadTablesWg.Wait()

	if isNeedTar.Load() == true {
		return s.fileWriter.Tar(dataDir)
	}

	return nil
}

func (s *receiver) downloadData(table *entity.Table) {
	s.logger.Debugf("Download data for table %s", table.Name)
	tableOffset, getOffsetErr := s.downloadedDataRepo.GetLatestDownloadedIdByTable(table.Name)
	if getOffsetErr != nil {
		s.logger.Errorf("can't load offset: %#v", getOffsetErr)
		return
	}

	getRowsErr := s.tableRowsRepo.GetWithLimitOffset(table, tableOffset, s.batchSize)
	if getRowsErr != nil {
		s.logger.Errorf("can't load rows with offset %d: %#v", tableOffset, getOffsetErr)
		return
	}
}

func (s *receiver) setOffset(table *entity.Table) {
	if table.LatestOffset != 0 {
		setOffsetErr := s.downloadedDataRepo.SetLatestDownloadedIdByTable(table.Name, table.LatestOffset)
		if setOffsetErr != nil {
			s.logger.Errorf("can't set offset %d for table %s: %#v", table.LatestOffset, table.Name, setOffsetErr)
			return
		}
	}
}

func (s *receiver) Shutdown() error {
	s.isStopped <- true
	return nil
}
