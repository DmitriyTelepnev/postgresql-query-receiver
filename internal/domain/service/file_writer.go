package service

import "bitbucket.org/DmitriyTelepnev/postgresql-query-receiver/internal/domain/entity"

type FileWriter interface {
	SetDestDataPath(path string) error
	CreateCurrentDataDir() (string, error)
	Tar(dataDir string) error
	WriteTable(path string, table *entity.Table) error
}
