package repository

import "bitbucket.org/DmitriyTelepnev/postgresql-query-receiver/internal/domain/entity"

type TableRows interface {
	GetWithLimitOffset(table *entity.Table, offset int64, limit int64) error
}
