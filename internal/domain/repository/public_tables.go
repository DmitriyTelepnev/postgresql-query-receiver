package repository

import "bitbucket.org/DmitriyTelepnev/postgresql-query-receiver/internal/domain/entity"

type PublicTables interface {
	GetAll() ([]*entity.Table, error)
}
