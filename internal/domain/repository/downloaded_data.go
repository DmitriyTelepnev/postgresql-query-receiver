package repository

type DownloadedData interface {
	GetLatestDownloadedIdByTable(tableName string) (int64, error)
	SetLatestDownloadedIdByTable(tableName string, offset int64) error
}
