package entity

type Table struct {
	Name         string
	Data         string
	LatestOffset int64
}
