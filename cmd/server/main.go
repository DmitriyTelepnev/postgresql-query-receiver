package main

import (
	"sync"

	"bitbucket.org/DmitriyTelepnev/postgresql-query-receiver/internal/infrastructure/migrations"

	"bitbucket.org/DmitriyTelepnev/postgresql-query-receiver/internal/infrastructure/service"

	"bitbucket.org/DmitriyTelepnev/postgresql-query-receiver/internal/infrastructure/datasource"
	"bitbucket.org/DmitriyTelepnev/postgresql-query-receiver/internal/infrastructure/repository"

	"bitbucket.org/DmitriyTelepnev/postgresql-query-receiver/internal/app"
	"bitbucket.org/DmitriyTelepnev/postgresql-query-receiver/internal/infrastructure/config"
	"bitbucket.org/DmitriyTelepnev/postgresql-query-receiver/pkg/infrastructure/logger"
	"bitbucket.org/DmitriyTelepnev/postgresql-query-receiver/pkg/infrastructure/signal"
)

func main() {
	cfg := config.MustConfigure()
	log := logger.NewStdout(cfg.Log.Level)

	pgConn, pgConnErr := datasource.NewPostgresConn(cfg.PgConn)
	defer pgConn.Close()
	exitOnError(log, pgConnErr)

	migrations.MustUp(pgConn, cfg.NodeName, log)

	publicTablesRepo := repository.NewPublicTables(pgConn, log)
	downloadedDataRepo := repository.NewDownloadedData(pgConn, log)
	tableRowsRepo := repository.NewTableRows(pgConn, cfg.NodeName, log)

	fileWriter := service.NewFileWriter(cfg.NodeName)
	exitOnError(log, fileWriter.SetDestDataPath(cfg.DestDirPath))

	receiverService := app.NewReceiver(
		cfg.DownloadPeriod,
		cfg.BatchSize,
		publicTablesRepo,
		downloadedDataRepo,
		tableRowsRepo,
		fileWriter,
		log,
	)

	mainWg := &sync.WaitGroup{}
	withWaitGroup(mainWg, receiverService.Run, log)

	signalHandler := signal.NewHandler(func() error {

		err := receiverService.Shutdown()
		return err
	})

	shutdownError := signalHandler.Poll()
	exitOnError(log, shutdownError)

	log.Info("service correctly stopped")
}

func withWaitGroup(wg *sync.WaitGroup, f func() error, logger logger.Logger) {
	wg.Add(1)
	go func() {
		defer wg.Done()
		exitOnError(logger, f())
	}()
}

func exitOnError(log logger.Logger, e error) {
	if e != nil {
		log.Fatalf("%s", e.Error())
	}
}
